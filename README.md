
Install buf cli v1.18.0 for your platform from
https://github.com/bufbuild/buf/releases

go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

PATH="${PATH}:${HOME}/go/bin"
