package main

import (
	"context"
	"flag"
	petv1 "gitlab.com/ivo100/proto/mypets/v1/pkg/pet/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"time"
)

var (
	addr = flag.String("addr", "localhost:50051", "the address to connect to")
	//name = flag.String("name", defaultName, "Name to greet")
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	petv1.PetStoreServiceServer
}

func main() {
	flag.Parse()
	// Set up a connection to the server.
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := petv1.NewPetStoreServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	log.Printf("client sending PutPet")

	response, err := client.PutPet(ctx, &petv1.PutPetRequest{
		PetType: petv1.PetType_PET_TYPE_DOG,
		Name:    "Bob",
	})
	if err != nil {
		log.Fatalf("PutPet error: %v", err)
	}
	log.Printf("PutPet response: %s", response.Pet)
	id := response.Pet.GetPetId()
	log.Printf("client sending GetPet")

	resp, err := client.GetPet(ctx, &petv1.GetPetRequest{PetId: id})
	if err != nil {
		log.Fatalf("GetPet error: %v", err)
	}
	log.Printf("GetPet response: %s", resp.Pet)

}
