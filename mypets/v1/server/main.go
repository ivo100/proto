package main

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	petv1 "gitlab.com/ivo100/proto/mypets/v1/pkg/pet/v1"
	"google.golang.org/grpc"
	"log"
	"net"
	"sync"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	petv1.PetStoreServiceServer
}

var store sync.Map

func hashed(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	petv1.RegisterPetStoreServiceServer(s, &server{})
	log.Printf("mypets server listening on %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (s *server) PutPet(ctx context.Context, in *petv1.PutPetRequest) (*petv1.PutPetResponse, error) {
	log.Printf("Received PutPet: %+v", in)
	id := hashed(fmt.Sprintf("%s-%s", in.PetType, in.Name))
	pet := petv1.Pet{
		PetId:   id,
		PetType: in.GetPetType(),
		Name:    in.GetName(),
	}
	resp := &petv1.PutPetResponse{
		Pet: &pet,
	}
	log.Printf("Storing pet with id: %s", id)
	store.Store(id, pet)
	return resp, nil
}

func (s *server) GetPet(ctx context.Context, in *petv1.GetPetRequest) (*petv1.GetPetResponse, error) {
	log.Printf("Received GetPet: %v", in.GetPetId())
	pet, ok := store.Load(in.GetPetId())
	if !ok {
		return nil, errors.New("not found")
	}
	pet1, ok := pet.(petv1.Pet)
	if !ok {
		return nil, errors.New("internal")
	}
	resp := &petv1.GetPetResponse{
		Pet: &pet1,
	}
	return resp, nil
}
