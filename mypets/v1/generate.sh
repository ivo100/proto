# buf breaking proto --against "<path>" &&
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
PATH="${PATH}:${HOME}/go/bin"
buf lint proto && buf generate proto
